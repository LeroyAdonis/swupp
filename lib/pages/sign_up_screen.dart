// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:lottie/lottie.dart';
import 'package:regexed_validator/regexed_validator.dart';
import 'package:swupp/pages/page_builder.dart';
import 'package:swupp/pages/sign_in_screen.dart';
import 'package:swupp/services/auth.dart';
import 'package:swupp/models/user.dart' as firebaseUser;
import 'package:swupp/services/database.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({super.key});

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  String fullName = '';
  String email = '';
  String password = '';
  String location = '';

  bool isValidEmail(String em) {
    String p = r'^[^@]+@[^@]+\.[^@]+';

    RegExp regExp = RegExp(p);

    return regExp.hasMatch(em);
  }

  bool _isLoading = false;

  final AuthService _auth = AuthService();
  final firebaseUser.User? user = firebaseUser.User();
  final DatabaseService data = DatabaseService();

  Future<void> checkLocationPermission() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      // Show dialog to inform the user that they need to change location settings manually
      // and provide a button to open the app settings
      // ignore: use_build_context_synchronously
      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text("Location Access Required"),
          content: Text("Please enable location access in the app settings."),
          actions: [
            TextButton(
              child: Text("Open Settings"),
              onPressed: () async {
                await Geolocator.openAppSettings();
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } else if (permission == LocationPermission.denied) {
      // Request the location permission again
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permission still not granted after requesting again,
        // handle the case as needed (e.g., show an error message)
      } else if (permission == LocationPermission.deniedForever) {
        // Permission denied forever after requesting again,
        // handle the case as needed (e.g., show an error message)
      }
    }
  }

  Future<void> getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    List<Placemark> placemarks = await placemarkFromCoordinates(
      position.latitude,
      position.longitude,
    );
    Placemark placemark = placemarks.first;

    setState(() {
      location = placemark.locality ?? '';
    });
  }

  Future<void> _submit() async {
    setState(() {
      _isLoading = true;
    });
    try {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState?.save();

        await checkLocationPermission();

        await getCurrentLocation();

        await _auth.registerWithEmailAndPassword(
            fullName, email, password, location);
        await addUserDetails();
        setState(() {
          _isLoading = false;
        });
        // ignore: use_build_context_synchronously
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const PageBuilder(),
          ),
        );
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> addUserDetails() async {
    await FirebaseFirestore.instance.collection('users').add({
      'full name': fullName,
      'email': email,
      'location': location,
    });
  }

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
    nameController.dispose();
    lastNameController.dispose();
    locationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _isLoading
          ? const CircularProgressIndicator(
              backgroundColor: Color.fromRGBO(244, 247, 252, 1),
            )
          : Scaffold(
              backgroundColor: Color.fromRGBO(244, 247, 252, 1),
              body: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 25,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            LottieBuilder.network(
                              width: 250,
                              'https://assets4.lottiefiles.com/packages/lf20_mjlh3hcy.json',
                            ),
                            Text(
                              'Sign up',
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.headlineMedium,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: TextFormField(
                              onChanged: ((value) {
                                setState(() => fullName = value);
                              }),
                              controller: nameController,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person),
                                border: OutlineInputBorder(),
                                hintText: 'Full Name',
                                hintStyle:
                                    TextStyle(color: Colors.grey.shade400),
                                filled: true,
                                fillColor: Colors.white,
                              ),
                              validator: (input) {
                                if (input!.isEmpty) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                  return 'Please enter your full name';
                                }
                                return null;
                              },
                              onSaved: (input) => fullName = input!,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: TextFormField(
                              controller: emailController,
                              onChanged: ((value) {
                                setState(() => email = value);
                              }),
                              decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.email_outlined),
                                border: const OutlineInputBorder(),
                                hintText: 'Email',
                                hintStyle:
                                    TextStyle(color: Colors.grey.shade400),
                                filled: true,
                                fillColor: Colors.white,
                              ),
                              validator: (input) {
                                if (input!.isEmpty) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                  return 'Please enter your email';
                                }
                                if (!validator.email(input)) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                  return 'Provide a valid email address';
                                }
                                return null;
                              },
                              onSaved: (input) => email = input!,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: TextFormField(
                              controller: passwordController,
                              onChanged: ((value) {
                                setState(() => password = value);
                              }),
                              decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.lock),
                                border: const OutlineInputBorder(),
                                hintText: 'Password',
                                hintStyle:
                                    TextStyle(color: Colors.grey.shade400),
                                filled: true,
                                fillColor: Colors.white,
                              ),
                              obscureText: true,
                              validator: (input) {
                                if (input!.isEmpty) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                  return 'Please enter your password';
                                }
                                return null;
                              },
                              onSaved: (input) => password = input!,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: TextFormField(
                              controller: locationController,
                              onChanged: ((value) {
                                setState(() => location = value);
                              }),
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.location_pin),
                                border: OutlineInputBorder(),
                                hintText: 'Location',
                                hintStyle:
                                    TextStyle(color: Colors.grey.shade400),
                                filled: true,
                                fillColor: Colors.white,
                              ),
                              validator: (input) {
                                if (input!.isEmpty) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                  return 'Please enter your location';
                                }
                                return null;
                              },
                              onSaved: (input) => location = input!,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          height: 50,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Color(0xff62cdf6),
                              minimumSize: const Size.fromHeight(50),
                            ),
                            onPressed: _submit,
                            child: _isLoading
                                ? const CircularProgressIndicator()
                                : const Text('Sign up'),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Already have an account?"),
                            TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => const Login(),
                                  ),
                                );
                              },
                              child: Text('Sign in'),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
