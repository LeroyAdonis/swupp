import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class IntroPage2 extends StatelessWidget {
  const IntroPage2({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xffF4F7FB),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'How It Works',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 50,
          ),
          LottieBuilder.network(
            width: 250,
            'https://assets7.lottiefiles.com/packages/lf20_prbklvk6.json',
          ),
          const SizedBox(
            height: 20,
          ),
          const Padding(
            padding: EdgeInsets.all(25.0),
            child: Text(
              "Simply create an account, add your items, browse the listings, and send the owner a message to arrange a trade.",
              style: TextStyle(fontSize: 24),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
