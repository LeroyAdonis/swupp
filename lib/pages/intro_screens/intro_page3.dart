import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:swupp/pages/sign_in_screen.dart';
import 'package:swupp/pages/sign_up_screen.dart';

class IntroPage3 extends StatelessWidget {
  const IntroPage3({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xffF4F7FB),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Benefits of Using Swupp',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 50,
          ),
          LottieBuilder.network(
            width: 200,
            'https://assets9.lottiefiles.com/packages/lf20_H1gFkyNn8t.json',
          ),
          const SizedBox(
            height: 10,
          ),
          const Padding(
            padding: EdgeInsets.all(25.0),
            child: Text(
              "Swupp is a great way to save money, reduce waste, meet new people, and support local businesses.",
              style: TextStyle(fontSize: 24),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: const Size.fromHeight(50),
                backgroundColor: Color(0xff62cdf6),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const RegistrationScreen(),
                  ),
                );
              },
              // ignore: prefer_const_constructors
              child: Text(
                'GET STARTED',
                style: TextStyle(fontSize: 16),
              ),
            ),
          )
        ],
      ),
    );
  }
}
