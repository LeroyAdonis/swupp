// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class IntroPage1 extends StatelessWidget {
  const IntroPage1({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xffF4F7FB),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Swap and Trade Without Cash',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          SizedBox(
            height: 50,
          ),
          LottieBuilder.network(
            width: 200,
            'https://assets8.lottiefiles.com/packages/lf20_u6ohgj2k.json',
          ),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Text(
              "Swap or trade goods or services.",
              style: TextStyle(fontSize: 24),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
